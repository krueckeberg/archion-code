<?php
declare(strict_types=1);
use Archion\{PreStatment};

$dsn = 'mysql:dbname=archion;host=127.0.0.1';
$user = 'kurt';
$password = 'kk0457';

$pdo = new \PDO($dsn, $user, $password);

$stmt = new PreStatment($pdo, [ 'sql' => "insert into archives(bundesland, name, website_url) values(:bundesland, :name, :website_url)",
                          [\PDO::PARAM_STR, \PDO::PARAM_STR, \POD::PARAM_STR]
                        ]); 
                        
$stmt->execute(['bundesland' => 'Niedersachsen']);
