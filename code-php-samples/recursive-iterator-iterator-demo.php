<?php

function run_test(RecursiveArrayIterator $ait, int $mode = RecursiveIteratorIterator::LEAVES_ONLY)
{
  error_reporting(E_ALL & ~E_WARNING);  
  
  $it = new RecursiveIteratorIterator($ait, $mode);

  $prior_key = 'empty';

  foreach ($it as $key => $leaf) {

      $depth = $it->getDepth();

      echo "[Depth: $depth] ";

      while ($depth-- > 0)
         echo "\t";

      if (is_string($key))
          $key = "'$key'";

      echo "[$key => $leaf]", PHP_EOL;

      $prior_key = $key;
  }
}

function tests(array $a)
{
  $ait = new RecursiveArrayIterator($a);

/*
  echo "Printing the array:\n";

  print_r($a);

  echo "\nThe RecursiveIteratorIterator mode is LEAVES_ONLY. Parents are ignored.\n\n";

  run_test($ait, RecursiveIteratorIterator::LEAVES_ONLY);

 */
  echo "\nThe RecursiveIteratorIterator mode is SELF_FIRST: parents come before children.\n\n";

  run_test($ait, RecursiveIteratorIterator::SELF_FIRST);

/*
  echo "\nThe RecursiveIteratorIterator mode is CHILD_FIRST: children come before parents.\n\n";

  run_test($ait, RecursiveIteratorIterator::CHILD_FIRST);
 */
}
/*
$array = [
 'A',
 'B',
 'C' => [
     'D',
     'E',
     'F' => [
         'G',
         'H'
        ]
     ],
 'I',
 'J'
];

tests($array);

$array = array(
    array(
        array(
            array(
                'leaf-0-0-0-0',
                'leaf-0-0-0-1'
            ),
            'leaf-0-0-0'
        ),
        array(
            array(
                'leaf-0-1-0-0',
                'leaf-0-1-0-1'
            ),
            'leaf-0-1-0'
        ),
        'leaf-0-0'
    )
);

echo "\n------------------\n";

tests($array);
*/

$yml = file_get_contents("./yml/vol2-image355.yml");

$array = \yaml_parse($yml);

tests($array);
