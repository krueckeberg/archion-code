<?php
declare(strict_types=1);

/*
FilesystemIterator::CURRENT_AS_PATHNAME
Makes FilesystemIterator::current() return the pathname.

FilesystemIterator::CURRENT_AS_FILEINFO
Makes FilesystemIterator::current() return an SplFileInfo instance.

FilesystemIterator::CURRENT_AS_SELF
Makes FilesystemIterator::current() return $this (the FilesystemIterator).

FilesystemIterator::CURRENT_MODE_MASK
Masks FilesystemIterator::current()

FilesystemIterator::KEY_AS_PATHNAME
Makes FilesystemIterator::key() return the pathname.

FilesystemIterator::KEY_AS_FILENAME
Makes FilesystemIterator::key() return the filename.

FilesystemIterator::FOLLOW_SYMLINKS
Makes RecursiveDirectoryIterator::hasChildren() follow symlinks.

FilesystemIterator::KEY_MODE_MASK
Masks FilesystemIterator::key()

FilesystemIterator::NEW_CURRENT_AND_KEY
Same as FilesystemIterator::KEY_AS_FILENAME | FilesystemIterator::CURRENT_AS_FILEINFO.

FilesystemIterator::OTHER_MODE_MASK
Mask used for FilesystemIterator::getFlags() and FilesystemIterator::setFlags().
 */

$start_dir = '/home/kurt/adocs-4-genealogy/modules/';

$dirIter = new \RecursiveDirectoryIterator($start_dir, \FilesystemIterator::KEY_AS_PATHNAME | \FilesystemIterator::CURRENT_AS_FILEINFO  | \FilesystemIterator::SKIP_DOTS);
	
$iter = new \RecursiveIteratorIterator($dirIter);

$callback_iter = new \CallbackFilterIterator($iter, function ($current, $key, $iterator) : bool {
	
   return $current->isFile() && $current->getExtension() ===  'yml';
});

foreach($iter as $pathname => $file_info) {


     $b = $file_info->isFile();

     //echo "Is file: ". ($b ? "true" : "false") . "\n";

     if (!$b) 
       continue;

     echo "Pathname: $pathname\n";  // <-- path + filename
     echo "Path:      " . $file_info->getPath() . "\n";  // <-- path only

/*
$file_info->getFilename(".yml") -- returns filename sans extensions.
 */
     echo "Basename:  " . $file_info->getBasename() . "\n";
     echo "Filename:  " . $file_info->getFilename() . "\n";
     echo "Extension: " . $file_info->getExtension() . "\n";
     
}

