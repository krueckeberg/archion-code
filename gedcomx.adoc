== GECOM X

=== Understanding What Gedcom X is

https://www.familysearch.org/developers/docs/guides/gedcom-x[What is
Gedcom X?] is the definitive introduction. It has helpful links that
will guide you to a better understanding of what Gedcom X is and how it
works.

The http://gedcomx.org/Documentation.html[GedcomX.org documentation]
page has links to the Gedcom X Conceptual Model. The GEDCOM X
https://github.com/FamilySearch/gedcomx/blob/master/specifications/conceptual-model-specification.md[Conceptual
Model] is a specification of formal concepts and types that are used to
provide a standard model and vocabulary for describing genealogical
data. Genealogical data is structured by data types such as persons,
relationships, and sources.

There is also a Gedcom X http://gedcomx.org/Recipe-Book.html[Recipe
Book] that has shows a few examples of how the Gedcomx X model is used.

List of main http://gedcomx.org/schemas.html[Gedcom X object types]

The
https://www.familysearch.org/developers/docs/guides/FamilyTree-data-objects[Family
Tree Data Model] is intended to help programmers become familiar with
the data model of the FamilySearch Family Tree, which closely conform to
the GedcomX http://www.gedcomx.org/Specifications.html[specification].

The differences between Events and Facts in Gedcom X is discussed here:

TIP: To easily test the ideas found in the first link below, simply go
to **Family Tree -> Tree*, and do `+ Event` to add an event, or `+ Fact`
to add a fact.

=== Events versus Facts

Events and facts are separately distinguished in Gedcom X. Events are
occurances viewed abstractly apart from the persons in those events.
Events often refer to persons and might infer relationships, but *events
are described independently of those persons and relationships.*

A `fact' is a data item that is presumed to be true about a specific
subject, such as a person or relationship. A time or place is often, but
not always, applicable to a fact. *Facts do not exist outside the scope
of the subject to which they apply.*:w

* https://github.com/FamilySearch/gedcomx/commit/ef0fe00c3645d2809c68cbccc39353d2154f9b23[Clarifying
Events vs Facts]
* Section 2.5.2 of
https://github.com/FamilySearch/gedcomx/blob/master/specifications/conceptual-model-specification.md[Events
and Facts Distinguished in Detail] is very good.

==== Event types specification

See Gedcom X
https://github.com/FamilySearch/gedcomx/blob/master/specifications/event-types-specification.md[events
specification] in Gedcom X Conceptual Model.

==== Fact types specification

See Gedcom X
https://github.com/FamilySearch/gedcomx/blob/master/specifications/fact-types-specification.md[facts
specification] in Gedcom X Conceptual Model.

==== GedcomX Recipe Book Examples

http://gedcomx.org/Recipe-Book.html[Recipe Book]

=== Serialization

The
https://github.com/FamilySearch/gedcomx/blob/master/specifications/json-format-specification.md[GEDCOM
X JSON Serialization Format] specifies a JSON media type for the GEDCOM
X Conceptual Model, and requests discussion and suggestions for
improvements. Highlighted JSON schema for each of the JSON objects can
be see at the GedcomX JSON
https://www.familysearch.org/developers/docs/api/gx_json[serialization
format page]. It also gives each object’s properties, any superclasses
and interfaces are also given.

There is also an
https://github.com/FamilySearch/gedcomx/blob/master/specifications/xml-format-specification.md[GEDCOM
X XML Serialization Format].

=== Software

* https://github.com/FamilySearch/gedcom5-conversion[GEDCOM 5.5 to
GEDCOM X Converter].
* The http://rs.gedcomx.org/[GEDCOM X Web services extension]
(i.e. ``GEDCOM X RS'') defines a standard interface to genealogical data
applications for the World Wide Web.
* https://github.com/FamilySearch/gedcomx-viewer[GedcomX-viewer]. Clone
it and do `open index.html'. You can add persons and download the tree
(to .json).
