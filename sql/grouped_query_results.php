<?php
declare(strict_types=1);
namespace Archion;
use Archion\Config;

include "../vendor/autoload.php";

$c = new Config;
$config = $c->configuration;

$pdo = new \PDO($config['dsn']);

$stmt = $pdo->query('select pn.id as place_names_id, p.address
     from place_names as pn
     inner join places as p
     on pn.id=p.placename_id');

// Return an array that maps place_names.id to an array of all addresses belonging to this id and locality.
$results = $stmt->fetchAll(\PDO::FETCH_GROUP|\PDO::FETCH_COLUMN);

print_r($results);

$stmt = $pdo->query('select pn.id as place_names_id, GROUP_CONCAT(p.address)
     from place_names as pn
     inner join places as p
     group by pn.id
     on pn.id=p.placename_id');

$results = $stmt->fetchAll(\PDO::FETCH_FUNC, function (int $id, string $addresses) { 
              return array($id => explode(',', $addresses));
          });

print_r($results);

