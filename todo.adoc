= TODO

== yml

Finish and double-check petzen-band2-image314.yml

== Code Changes

Add these methods for debugging help:

* `Image::__toString : string`
* `Event::__toString : string`
* `Person::__toString : string`
* `Relationship::__toString : string`

== Prospective Changes

These may have already been incorporated?

`~/db/m/code/p/yml-general-settings.yml` has now only two sections: `register_shortnames` and `place_names`.
The `register_shortnames` looks something like this:

We'll just use the locality names found in `place_names:` section of the `general-settings.yml` file
 in the individual fact-containing `.yml` files.
+
All the place names including the official, canoncial version of the place name, and what it is
an alias of, must already be in the database before we process the `.yml` files. 

Then as we read and process the `.yml` files and encounter a reference to one of the localities (`locality:`)
found in the `general-settings.yml` file, along with an address (`address:`), we will check if it is already
in the `places` table; if not, we will insert it.

To do this, we will create a hashtable that maps `place_names.localty` to `place_names.id`.

The code will validate the the `locality` found in a fact in a fact-containing `yml` file does actually exist.

If not, it will throw an exception. If it does exist, it will check whether the unique pair (`places.placename_id`,
`places.address`) exists. If it does, nothing needs to be done. To check whether this pair, it will check a
hashtable that maps `places.placename_id` to an array of addresses.

If it does not exist, it will insert the new address into `places` and add the new address to the array of addresses
corresponding to the `places.placename_id`.



