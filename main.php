<?php
declare(strict_types=1);
use Archion\{YmlProcessor, Config};

include "vendor/autoload.php";

$c = new Config;

$config = $c->configuration;

$processor = new YmlProcessor($config);

foreach($processor as $pathname => $file_info) {
     
     try {  

       echo "Processing $pathname.\n";
       
       $processor($pathname, $file_info);

       echo "Processed: $pathname. \n";

     } catch(\Exception $e) {

         echo $e->getMessage() . "\n";
         echo "Error occurred at " . $e->getFile() . " on line " . $e->getLine();
         return;
     } 
}
