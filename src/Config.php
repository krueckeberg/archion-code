<?php
declare(strict_types=1);
namespace Archion;

readonly class Config {

  public array $configuration;

  function __construct()
  {
      static $filename = "config.yml";

      $this->configuration = \yaml_parse_file(__DIR__ . "/$filename");
  } 
}
