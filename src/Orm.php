<?php
declare(strict_types=1);
namespace Archion;

class Orm {   

   private \SplObjectStorage $st;

   private \PDO $pdo;

   private array $placeNamesMap; //  place_names.locality => place_names.id

   /* 
    * table_name => \PDOStatement insert 
    */ 
   private array $insert_stmts; 

   function __construct(\PDO $pdo, array $config)
   {
      $this->pdo = $pdo;
      
      $placeNamesMgr = new PlaceNamesMgr($this->pdo, $config);

      $this->placeNamesMap = $placeNamesMgr->readPlaceNamesMap();
      
      $this->st = new \SplObjectStorage();

      foreach ($config['insert_stmts'] as $table_name => $insert_stmt) {

          $stmt = $this->pdo->prepare($insert_stmt);

          $this->insert_stmts[$table_name] = $stmt;     
      }
   }

   function saveImage(Image $image) : int
   {
      $primaryKey = $this->insertImage($image);
      
      foreach ($image->events as $event) {

         $this->saveEvent($event, $primaryKey);
      }
      
      return $primaryKey;
   }

   /* Prepared statement:
   *  insert into images(image_num,
              lpage_num,
              permalink,
              register_id,
              ymlfile) values(:register_id, :image_num, :lpage_num, :permalink, :register_id, :ymlfile)
    */
   private function insertImage(Image $image) : int 
   {
      $stmt = $this->insert_stmts['images'];
      
      echo "DEBUG: Saving image for yml file {$image->ymlfile}.\n";
      
      $rc = $stmt->execute(['image_num' => $image->image_num,
            'lpage_num' => $image->lpage_num,
            'permalink' => $image->permalink,
            'register_id' => $image->register_id,
            'ymlfile' => $image->ymlfile]
            ); 

     $primaryKey = (int) $this->pdo->lastInsertId(); 
     
     return  $primaryKey;
   }

   /* PDOStatement to insert into events table:
    *   insert into events(event, view_date, evdate, entry_num, place_id, image_id)
    *   values(:event, :view_date , :evdate, :entry_num, :place_id, :image_id) 
    */
   function saveEvent(Event $event, int $image_primKey) : bool
   {
      $image_id = $image_primKey; // $this->st[$event->owner];
      
      $place_id = $this->placeNamesMap[$event->locality];
      
      echo "DEBUG: Saving Event whose type is {$event->event}.\n";
    
      $this->insert_stmts['events']->execute(
                                       ['event' => $event->event,
 					'view_date' => $event->view_date,
 					'evdate' => $event->evdate,
 					'entry_num' => $event->entry_num,
					'place_id' => $place_id, 
					'image_id' => $image_id]);

      // Save primary key of new row
      $event_id = (int) $this->pdo->lastInsertId();
 
      // Save persons and relatioships
      foreach ($event->persons as $person) {
          
        $this->savePerson($person, $event_id);
      }
     
      foreach ($event->relationships as $relationship) {

         $this->saveRelationship($relationship, $event_id);
      }
   
      return true; 
   }

   function savePerson(Person $person, int $event_id) : int
   {
      $rc = $this->insert_stmts['event_persons']->execute(['given' => $person->given, 'surname' => $person->surname, 'gender' => $person->gender, 'event_id' => $event_id]);
 
      $person_id = (int) $this->pdo->lastInsertId();
 
      $this->st[$person] = $person_id;
      
      foreach ($person->facts as $fact) {
          
          $this->saveFact($fact, $person_id, $event_id);
      }
      
      return $person_id;
   }

   /*
   private function place_exists(string $address, string $locality) : bool
   {
      $placename_id = $this->placeNamesMap[$locality]; 
      
      $this->place_exists_query->execute(['address' => $place->address, 'placename_id' => $placename_id]);

      $count = $this->place_exists_query->fetchColumn(0);

      return ($count === 1) ? true : false;
   }
   */
   
     /*
    * insert into person_facts(person_id, event_id, type, fact_date, place_id)
    *  values(:person_id, :event_id, :type, :fact_date, :place_id)

    * IMPORTANT: PDOStatement::execute([...]) treats all the input array parameters as PDO_STR
    * parameters. This spells trouble when boolean input value, such as Fact::precise--which
    * we are no longer using--has a value of false. The solution is to convert
    * Fact::precise to an int.
    */
   function saveFact(Fact $fact, int $person_id, int $event_id) : int
   {
      if (isset($this->placeNamesMap[$fact->locality]) === false)
          throw new \ErrorException("locality of '{$fact->locality}' not found in Orm->placeNamesMap[{$fact->locality}]");

      $place_id = $this->placeNamesMap[$fact->locality];
      
      echo "DEBUG: Saving fact of type {$fact->type}.\n";
      echo $fact;
      
      $rc = $this->insert_stmts['person_facts']->execute([
           'type'      => $fact->type,
           'person_id' => $person_id,
           'event_id'  => $event_id,
           'date_type' => $fact->date_type, 
           'date'      => $fact->date,
           'place_id'  => $place_id]); 

      $fact_id = (int) $this->pdo->lastInsertId();
 
      return $fact_id; 
   }

   function saveRelationship(Relationship $r, int $event_id) : int
   {
      $person1_id = $this->st[$r->person1];

      $person2_id = $this->st[$r->person2];
      
      $rc = $this->insert_stmts['relationships']->execute(
               ['type'     => $r->type,
                'person1'  => $person1_id,
                'person2'  => $person2_id,
                'event_id' => $event_id ]);

      $primKey = (int) $this->pdo->lastInsertId();
   
      return $primKey;
   }
}                                    
