<?php
declare(strict_types=1);
namespace Archion;

class YmlProcessor implements \IteratorAggregate {

   private Repo $repo;

   private array $shortnameMap;
   private string $start_dir;
   private string $sub_dirs;
   private array  $config;
   
   public function __construct(array $config)
   {  
      $this->start_dir = $config['start_dir'];
      $this->subdirs = $config['sub_dirs'];
      
      $this->extension = 'yml';
 
      $this->repo = new Repo($config);

      $this->config = $config; 

      $this->shortnameMap = $this->repo->fetchShortnameToPrimKeyMap();   
   }

   function getIterator() : YmlFileIterator
   { 
      return new YmlFileIterator($this->config);
   }

   function __invoke(string $pathname, \SplFileInfo $yml_fileinfo)
   {
       $yml = \yaml_parse_file($pathname);

       $register_id = $this->shortnameMap[$yml['register']['shortname']];
       
       $image = new Image($yml['image'], $yml_fileinfo->getFilename(), $register_id);
       
       if ($this->repo->imageExists($image) === false) {

           $image->save($this->repo);
       }
   }

   /*
    * Convert and save the .yml file to a .json with the same name.
    */
   function write_asJson(string $event_yaml, $jsonBasename)
   {      
      $json = json_encode($event_yaml, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);
     
      $fname = $jsonBasename . ".json";
      
      file_put_contents($fname, $json);
   }
}
  
