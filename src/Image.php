<?php
declare(strict_types=1);
namespace Archion;

readonly class Image implements PersistInterface {   

   public int $image_num;

   public string $permalink;

   public int $lpage_num;

   public int $register_id; 
   
   public string $ymlfile;

   readonly \ArrayObject $events; 

   function __construct(array $image, string $ymlFilename, int $register_id)
   {
      $this->image_num = $image['image_num'];
     
      $this->permalink = $image['permalink'];
     
      $this->lpage_num = $image['lpage_num'];
      
      $this->ymlfile = $ymlFilename;

      $this->register_id = $register_id;

      $this->events = new \ArrayObject;

      foreach($image['events'] as $event) {

         $this->events[] = new Event($event, $this);
      }
   }                                 

   function __toString() : string
   {
      $str = 'image_num: ' . $this->image_num;

      $str .= 'permalink: ' . $this->permalink;
      
      $str .= 'lpage_num: ' . $this->lpage_num;
      
      $str .= 'ymlfile: ' . $this->ymlfile;

      $str .= 'register_id: ' . (string) $this->register_id;

      $str .= "------------------------\n";
      
      return $str;
   }
   
   function save(Repo $r) : int
   {
      return $r->save($this); 
   }
}                                    
                                     
                                     
                                     
                                     
                                     
                                     
                                     
                                     
                                     
                                     
                                     
                                     
                                     
                                     
                                     
                                     
                                     
                                     
                                     
                                     
                                     
                                     
                                     
                                     
                                     
                                     
                                     
                                     
                                     
                                     
                                     
                                     
                                     
                                     
                                     
                                     
                                     
                                     
                                     
                                     
                                     
                                     
                                     
                                     
                                     
                                     
                                     
                                     
                                     
                                     
                                     
                                     
                                     
                                     
                                     
                                     
                                     
                                     
                                     
                                     
                                     
                                     
