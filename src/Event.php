<?php
declare(strict_types=1);
namespace Archion;

readonly class Event implements PersistInterface {

  public string $event;

  public string $view_date;

  public string $locality; // Event date

  public string $evdate; // Event date

  public int $entry_num;

  // This array maps the person's yml 'key' to the Person instance.
  public \ArrayObject $persons; 
  
  public \ArrayObject $relationships;

  function __construct(array $event)
  {
    $this->event = $event['event'];

    $this->view_date = $event['view_date'];

    $this->evdate = $event['evdate'];

    $this->entry_num = $event['entry_num'];

    $this->locality = $event['locality'];
    
    $this->persons = new \ArrayObject;
    
    $this->relationships = new \ArrayObject;

    $this->addPersons($event);
    
    $this->addRelationships($event);
  }

  function __toString() : string
  {
      $str = "Event: " . $event;

      $str .= "view_date: " . $view_date;

      $str .= "locality: " .$locality; // Event date

      $str .= "event date: ". $evdate; // Event date

      $str .= "entry_number: " . $entry_num;

      $str .= "-------------------------------\n";

      return $str;
  }

  private function  addPersons(array $event) : void
  {
     foreach ($event['persons'] as $person) {

       $this->persons[$person['key']] = new Person($person,
                  $this);
     }
  }
  
  private function addRelationships($event) : void
  {

     foreach ($event['relationships'] as $ymlkey => $relationship) {

          $this->addRelationship($relationship);
     }   
  } 

  private function addRelationship(array $array)
  {
      $person1 = $this->persons[$array['person1']];
      $person2 = $this->persons[$array['person2']];

      $relationship = new Relationship($array['type'], $person1, $person2, $this);
      
      $this->relationships[] = $relationship;
  }
 
  function save(Repo $r) : int
  {
     return $r->saveEvent($this);
  }
}
