<?php
declare(strict_types=1);
namespace Archion;

class Repo {

   private \PDO  $pdo;

   private Orm $orm; 

   private array $shortNamesMap;  // Maps registers.shortname => registers.id.

   private array $config;

   private \PDOStatement $image_exists_query;

   function __construct(array $config)
   {
      $this->pdo = new \PDO($config['dsn']);
      
      $this->orm = new Orm($this->pdo, $config);
           
      $this->image_exists_query = $this->pdo->prepare($config['queries']['image_exists_query']);      

      $this->config = $config;
   }

   /*
    *  Returns a map of 'registers.shortname' to 'registers.id'
    */
   function fetchShortnameToPrimKeyMap() : array
   {
      /*
       *  Run query: select shortname, id from registers.
       *  The results are an array whose keys is 'registers.shortname' and
       *  whose value is 'registers.id'
       */
      $stmt = $this->pdo->query($this->config['queries']['shortname_ids']);
      
      $query_results = $stmt->fetchAll(\PDO::FETCH_GROUP|\PDO::FETCH_UNIQUE|\PDO::FETCH_COLUMN);
      
      /*
      foreach ($general_settings['register_shortnames'] as $shortname) {
      
         if (!isset($query_results[$shortname]))
            throw new \ErrorException("The Archion 'registers' table is missing the shortname: $shortname."); 
      
         $map_result[$shortname] = $query_results[$shortname]; 
      }
       
      return $map_result;
       * 
       */
      return $query_results;
   }

   function imageExists(Image $img) : bool
   {
      $this->image_exists_query->execute(['number' => $img->image_num]);

      $count = $this->image_exists_query->fetchColumn(0);

      return ($count === 1) ? true : false;
   }
 
   function save(Image $image)
   {  
      $this->pdo->beginTransaction();

      $primKey = $this->orm->saveImage($image);

      $this->pdo->commit();
      
      return $primKey;
   }
}
