<?php
declare(strict_types=1);
namespace Archion;

readonly class Relationship {

    public Person $person1; 
    public Person $person2;

    public string $type;
    
    public Event $event;

    function __construct(string $type, Person $person1, Person $person2, Event $owner) 
    {
       $this->type = $type;
       $this->person1 = $person1;
       $this->person2 = $person2;
       $this->event = $owner;
    }
}
