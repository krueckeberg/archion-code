<?php
declare(strict_types=1);
namespace Archion;

/* Sample code 
$dirIter = new RecursiveDirectoryIterator(
         '/home/kurt/adocs-4-genealogy/modules/',
          FilesystemIterator::KEY_AS_PATHNAME |
          FilesystemIterator::CURRENT_AS_FILEINFO |
          FilesystemIterator::SKIP_DOTS);

$iter = new RecursiveIteratorIterator($dirIter);

$filter_iterator = new FileExtensionFilterIterator($iter, "adoc");

foreach ($filter_iterator as $file) {
    
   echo $file->getPathname() . PHP_EOL;
   
}

*/
class FileExtensionFilterIterator extends FilterIterator {

     private string $extension;
    
     function accept() 
     {
         $file_info = $this->getInnerIterator()->current();
         
         return ($file_info->isFile() && ($file_info->getExtension() === $this->extension)) ? true : false;
     }

     function __construct(\Iterator $iter, string $ext)
     {
        parent::__construct($iter);

        $this->extension = $ext;
     }      
}
