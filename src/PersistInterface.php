<?php
declare(strict_types=1);
namespace Archion;

interface PersistInterface {
  
   function save(Repo $repo) : int;
   //function read(Repo $repo, mixed $criteria) : int;
}
