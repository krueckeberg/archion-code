<?php
declare(strict_types=1);
namespace Archion;

class PlacesMgr { 
 
   private \PDO $pdo;

   private array $localitytoPrimaryKey; // maps `place_names.locality` to `place_names.id`

   /* 
    * An array that maps `place_names.id` => [array of their associated addresses]   
    */
   private array $primaryKeyAddresses; 

   static private bool $initialized = false;
   
   static private \PDOStatement $insert_place;
                           
   static private \PDOStatement $countAddressPlaceNameID; // <-- Unused?
                                  
   function __construct(\PDO $pdo, array $config)
   {
      $this->pdo = $pdo;

      if (self::$initialized == false) {
        
        $this->localitytoPrimaryKey = $this->loadLocalitytoKeyMap($pdo, $config);

        self::$insert_place = $pdo->prepare($config['insert_stmts']['residences']);
    
        self::$countAddressPlaceNameID = $pdo->prepare($config['queries']['count_address_placename']);   
                
        self::$initialized = true;
      }

      // Get all addresses (as an array) associated with each place_names.id.
      $stmt = $pdo->query($config['queries']['all_addresses_at_place_names_id']);
      
      // Return an array that maps place_names.id to an array of all addresses belonging to this id and locality.
      $this->primaryKeyAddresses = $stmt->fetchAll(\PDO::FETCH_GROUP|\PDO::FETCH_COLUMN);
  }

  private function loadLocalitytoKeyMap(\PDO $pdo, array $config) : array
  {
     $mgr = new PlaceNamesMgr($pdo, $config);

     $settings = \yaml_parse_file($config['settings_file']); 

     // Insert any place_names that may have been added to general-settings['place_namses'] that perhaps
     // aren't already in the database.
     $mgr->insertPlaceNames($settings['place_names']);

     return $mgr->localityToKeyMap();
  }

  function insertAddressIfNew(string $locality, int $address) : void
  {
    $locality = \ucfirst($locality); // place_names.locality always has an uppercase first letter.

    if (isset( self::$localitytoPrmaryKey[$locality]) ) { // The locality must exist in the database

        $prim_key = $this->localitytoPrmaryKey[$locality];
        
        if (isset($this->primaryKeytoAddresses[$prim_key][$address])) { 

           return; // since the pair [$place_name, $address] already exists

        } else { // insert new address into database and add it to the addresses array for $prim_key

           $this->insertAddress($prim_key, $address);

           $this->primaryKeytoAddresses[$prim_key][] = $address; // append new address to array of addresses
        }

    } else {

       throw new \ErrorException("Localtiy $locality not found in database.\n");
    }  
 }

 private function insertAddress(int $place_names_id, int $address) : int
 {
    $rc = self::$insert_place->execute(['address' => $place['address'],
     			   'placename_id' => $place_names_id]);   
    
    $primKey = (int) $this->pdo->lastInsertId();

    return $primKey; 
 }
}
