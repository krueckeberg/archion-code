<?php
declare(strict_types=1);
namespace Archion;

readonly class Fact {

  public string $type;
  
  public string $date_type;
  
  public string $date;

  public string $details; // This is stored in `fact_details.details`
  
  public string $locality; 
  
  public Person $person; // the person to whom the fact applies.
 
  function __construct(array $fact, Person $owner)  
  {
     $this->type = $fact['type'];
          
     $this->date = (string) $fact['date'];
     
     $this->locality = \ucfirst($fact['locality']);
 
     $this->date_type = $fact['date_type'];

     $this->details = \array_key_exists('details', $fact) ? $fact['details'] : '';

     $this->person = $owner;     
  }

  function __toString() : string
  {
    $str = "Fact::type = {$this->type}\n";
    $str .= "Fact::date_type = {$this->date_type}\n";
    $str .= "Fact::fact_date = {$this->fact_date}\n";
    $str .= "Fact::details = {$this->details}\n";
    $str .= "Fact::locality = {$this->locality}\n";
    $str .= "-------------------------------------\n";
    return $str;
  }
}
