<?php
declare(strict_types=1);
namespace Archion;

class PlaceNamesMgr { 
 
   private \PDO $pdo;

   static private array $localitytoPrimaryKey; // A map of `place_names.locality` => `place_names.id`

   static private bool $initialized = false;

   static private array $config; 
   
   static private \PDOStatement $insertPlaceName;
                          
   static private \PDOStatement $updateAliasofId;
                                    
   function __construct(\PDO $pdo, array $config)
   {
      $this->pdo = $pdo;
 
      self::$config = $config;

      self::$localitytoPrimaryKey = $this->readPlaceNamesMap();
      
      if (!isset(self::$insertPlaceName)) {
      
        self::$insertPlaceName = $pdo->prepare($config['insert_stmts']['place_names']);
          
        self::$updateAliasofId = $pdo->prepare($config['queries']['update_aliasof_id']);
      }
   }

   function localityToKeyMap() : array
   {
      return self::$localitytoPrimaryKey;  
   } 

   /*
    * Returns a map that maps `place_names.locality` => `place_names.id`.
    */
   function readPlaceNamesMap() : array
   {     
     $stmt = $this->pdo->query(self::$config['queries']['all_placenames']);
     
     $query_results = $stmt->fetchAll(\PDO::FETCH_GROUP|\PDO::FETCH_UNIQUE|\PDO::FETCH_COLUMN);

     return $query_results;
  }

  function insertPlaceNames(array $placeNames) : array // returns self::$localitytoPrimaryKey
  {
     $this->pdo->beginTransaction();
    
     foreach ($placeNames as $place_name) {
  
        if (!isset(self::$localitytoPrimaryKey[$place_name['locality']])) {
  
           $primKey = $this->insert_placeName($place_name);
  
           self::$localitytoPrimaryKey[$place_name['locality']] = $primKey;
        }
     }
  
     $this->pdo->commit();

     return self::$localitytoPrimaryKey;
  }
 
  function insert_placeName(array $place_name): int
  {
    $rc = self::$insertPlaceName->execute(['locality' => $place_name['locality'], 'aliasof_id' => null]);
                
    $primKey = (int) $this->pdo->lastInsertId();
    
    if (isset(self::$localitytoPrimaryKey[$place_name['alias']])) {
       
       $aliasof_id = self::$localitytoPrimaryKey[$place_name['alias']];
       
    } else if ($place_name['alias'] == 'self') { // It must be 'self' or we have an terminal error.
       
       $aliasof_id = $primKey;        
       
    } else { // alias is missing, throw exception.
    
      throw new \Exception("Error in general_settings's place_name is missing 'alias:'.");
   }
      
   self::$updateAliasofId->execute(['aliasof_id' => $aliasof_id, 'id' => $primKey]);
   
   return $primKey;
 }
}
