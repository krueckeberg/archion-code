<?php
declare(strict_types=1);
namespace Archion;

/* HOWTO Example:

$dirIter = new RecursiveDirectoryIterator('/home/kurt/adocs-4-genealogy/modules/',
    FilesystemIterator::KEY_AS_PATHNAME | 
    FilesystemIterator::CURRENT_AS_FILEINFO |
    FilesystemIterator::SKIP_DOTS);

$iter = new RecursiveIteratorIterator($dirIter);

$filter_iterator = new CallableFilterIterator($iter, 
                     function(\FilterIterator $iter) : bool
  	             {
  	                $file_info = $iter->getInnerIterator()->current();
  
  	                return ($file_info->isFile() && ($file_info->getExtension() === 'yml')) ? true : false;
  	             });

foreach ($filter_iterator as $file) {
    
   echo $file->getPathname() . PHP_EOL;
}
*/

class CallableFilterIterator extends \FilterIterator {

     private $func; // type is `callable`
    
     function accept() 
     {
       /* 
         ($this->func)($this) below will do something like these two lines:

         $file_info = $this->getInnerIterator()->current();
         
         return ($file_info->isFile() && ($file_info->getExtension() === $this->extension)) ? true : false;
        */ 

        return ($this->func)($this);  

     }

     function __construct(\Iterator $iter, callable $func)
     {
        parent::__construct($iter);

        $this->func = $func;
     }      
}
