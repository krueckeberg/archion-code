= Design

See comments in `Events::save()`.

== Comments on RecursiveIteratorIterator

The modes passed to RecursiveIteratorIterator don't affect the
keys and associated values returned in a foreach loop, rather it
controls what happends when a nested structure, say, an array is
encountered.

`SELF_FIRST` 

`SELF_FIRST` results in the parent key being returned first when a nested structure
is encountered; for example, if nested array is encountered, a foreach-loop
will initially return the parent key. The associated value returned is the
entire array. This nested subarray is the first "child" subarray. The next iteration
of the loop returns the elements of the array.

For exampe, this array Example:

[,php]
----
$array = [
  'A',
  'B',
  'C' => [
      'D',
      'E',
      'F' => [
          'G',
          'H'
         ]
      ]
]; 
  
$it = new RecursiveIteratorIterator(new RecursiveArrayIterator($a), RecursiveIteratorIterator::SELF_FIRST);

foreach ($it as $key => $value) {

    $depth = $it->getDepth();

    echo "[Depth: $depth] ";

    while ($depth-- > 0)
       echo "\t";

    echo "[$key => $value]", PHP_EOL;
}
----

gives this result

----
[Depth: 0] [0 => A]
[Depth: 0] [1 => B]
[Depth: 0] [C => Array]
[Depth: 1] 	[0 => D]
[Depth: 1] 	[1 => E]
[Depth: 1] 	[F => Array]
[Depth: 2] 		[0 => G]
[Depth: 2] 		[1 => H]
----

The key 'C' of the first-encountered subarray is returned, and it associated value is the entire array;
likewise, when the deeper, second nested array, whose key is 'F', is encountered, 'F' is first returned
along with the associated value of the entire array.

The subarrays are considered "children" of their containing array.

----
[Depth: 0] [0 => A]
[Depth: 0] [1 => B]
[Depth: 1] 	[0 => D]
[Depth: 1] 	[1 => E]
[Depth: 2] 		[0 => G]
[Depth: 2] 		[1 => H]
[Depth: 1] 	[F => Array]
[Depth: 0] [C => Array]
----

`CHILD_FIRST`

`CHILD_FIRST` causes a nested structure like an array to be immediately entered and its keys and 
values to be returned. Only after the individuals keys and values of the nested arrays are
return will the parent key be returned along with the entire associated array (that has already
been iterated).

`LEAVES_ONLY`

`LEAVES_ONLY` never returns the keys of nested structures. In our example, neither the keys of the
first nested array nor its subarray will be returned:

----
[Depth: 0] [0 => A]
[Depth: 0] [1 => B]
[Depth: 1] 	[0 => D]
[Depth: 1] 	[1 => E]
[Depth: 2] 		[0 => G]
[Depth: 2] 		[1 => H]
----

=== The Goal

The design should be driven by what each class's `save(Repo $r)` 
method will require. This will come from the SQL insert statements.
Take for example,

[,yml]
----
persons:
  - key: father1 
    given: Johann Heinrich
    surname: Jäger
    gender: male
  - object_hash: &son 'son'
    given: Johann Heinrich
    surname: Jäger    
    gender: male
  - object_hash: &father2 'father2'
    given: Carl Heinrich Gottlieb
    surname: Krückeberg
    gender: male
  - object_hash: &daughter 'daughter'
    given: Philippine Christine
    surname: Krückeberg
    gender: female
relationships:
  - type: parent_of
    person1: *father1
    person2: son
  - type: parent_of
    person1: *father2
    person2: daughter
  - type: husband_of
    person1: *son
    person2: daughter
----

Because residence and jobs fact need to reference the shared address or
job held, the `addresses` and `jobs` or `jobs_held` block needs to come
before the `facts`. The `object_hash` -- or another name like `object_key` --
can be used as **key** of its element in `Event::$shared_addresses`.

And we can make the individual Facts in the facts array to be either a `stdClass`
or itself an array. Doing this will allow an array fact to simply have another
key-/value pair like, say, `address_key => 'nordholz'`.

The `relationships` table requires has two attributes that are foreign keys:
`person1` and `person2`. When `relationships[0]` is to be save, i.e., inserted,
into the database, it will look up the `event_persons.id` primary keys for
`father1` and `son`:

[,php]
----
// father1 is saved to db
$father1->save($repo);

function Person::save(Repo $r)
{
  /*
   $this->person is an array with the `EventPerson` attribute
   values.

   The next statements below may be done by an Orm component?
   */
  
  $prepared_stmt->execute($this->person); 

  $father1_primary_key = $pdo->lastInsertId();

  $this->ymlKeytoPrimaryKeyMap[$this->yml_key] = $father1_primary_key;
}
----

=== Object Creation

To track the `object_hash` and the PHP objects associated with it, use the Factory
Pattern instead of `new` to create the objects--maybe to save them, too.

=== Client Classes

The client objects are:

A single `Register` has `Image` 's, which has `Event` 's of interest, which contain
`Participant` 's, who have associated `Fact` 's about them and who also are part
of `Relationship` 's.

Some `Fact` 's have additional information besides the fact `type` and the `person`
to whom it applies. Address, for example, has `locality` and `number` in addition
to the `type` of `address` and the `Person` who lives at it. It is sort of a
decorated fact.
